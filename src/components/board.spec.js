import React from 'react';
import { shallow } from 'enzyme';
import Board from './board';
import Square from './square';

it('Board renders 9 squares', () => {

  const squares = Array(9).fill(null);
  const onClick = 'onClick';
  const board = shallow(<Board squares={squares} onClick={onClick}/>)
  expect(board.find(Square)).toHaveLength(9)

})