import React from 'react';
import { shallow } from 'enzyme';
import Square from './square';
import renderer from 'react-test-renderer';

describe('Square', () => {
  /*it('renders a button', () => {
    const value = 'value';
    const onClick = 'onClick';
    const square = shallow(<Square value={value} onClick={onClick}/>);
    const button = <button className="square" onClick={onClick}>{value}</button>;

    expect(square).toContainReact(button);
  })*/
  const value = 'value';
  const onClick = 'onClick';
  const square = renderer.create(<Square value={value} onClick={onClick}/>);
  let tree = square.toJSON();

  it('renders a button', () => {
    expect(tree).toMatchSnapshot();
  })

  it('adds active class on mouse over', () => {
    tree.props.onMouseOver();
    tree = square.toJSON();
    expect(tree).toMatchSnapshot();
    expect(square.getInstance().state.classNames).toContain('active');
  })

  it('removes active class on mouse leave', () => {
    tree.props.onMouseLeave();
    tree = square.toJSON();
    expect(tree).toMatchSnapshot();
    expect(square.getInstance().state.classNames).not.toContain('active');

  })


})
