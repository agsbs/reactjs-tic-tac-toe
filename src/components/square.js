import React from 'react';

export default class Square extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      classNames: ['square']
    }
  }

  onMouseOver() {
    let classNames = this.state.classNames.slice();
    classNames.push('active');

    this.setState({
      classNames: classNames
    })
  }

  onMouseLeave() {
    const classNames = this.state.classNames.filter((value) => value !== 'active');

    this.setState({
      classNames: classNames
    })
  }

  render() {
    return (
      <button className={this.state.classNames.join(' ')}
              onClick={this.props.onClick}
              onMouseOver={() => {this.onMouseOver()}}
              onMouseLeave={() => {this.onMouseLeave()}}
      >
        {this.props.value}
      </button>
    );
  }

}